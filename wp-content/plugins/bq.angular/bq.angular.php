<?php

if (!defined('BQ_ANGULAR_DIR'))
{ define('BQ_ANGULAR_DIR', untrailingslashit(plugin_dir_path(__FILE__))); }

/*
Plugin Name: BlockQuote Angular
Plugin URI: http://www.blockquote.com.au
Description: This is a porting plugin for the angular components working within wordpress
Author: Hilary Osborne
*/

class BQ_Angular {

    public $widgets = [
        'contact.form' => [
            'code' => 'contact.form',
            'title' => 'Simple Contact Form',
            'description' => 'Contains the contact form',
            'handler' => 'BQ_Widget_Contact',
            'ng' => [
                'selector' => 'embed-contact',
                'system' => 'embed.contact'
            ]
        ]
    ];

    public function __construct() {

        $this->loadVendorLibraries();

        $this->loadCoreClasses();

        $this->loadWidgetHandlers();

        $this->loadPageClasses();

        $this->loadAdminPages();

        $this->loadShortCodes();

        $this->loadJsScripts();

        $this->loadCssScripts();
    }

    public function loadAdminPages() {

        add_action('admin_menu', function(){
            $page = new BQ_Page_Admin();
            $page->plugin = $this;
            add_menu_page( 'BQ Angular', 'BQ Angular', 'manage_options', 'bq-angular', [$page, 'renderPage']);
        });

    }

    public function loadPageClasses() {
        // Retrieve the context director
        $dir = untrailingslashit( plugin_dir_path(__FILE__ ) );
        // Recurssively load the directory
        $this->_Recusive_Load_Dir($dir.'/pages');

        acf_add_options_sub_page([
            'page_title' => 'Core Settings',
            'menu_title' => 'Core Settings',
            'menu_slug' => 'blqt-angular-core',
            'capability' => 'edit_posts',
            'parent_slug' => 'bq-angular',
            'position' => false,
            'icon_url' => false
        ]);

        acf_add_options_sub_page([
            'page_title' => 'Contact Form',
            'menu_title' => 'Contact Form',
            'menu_slug' => 'blqt-angular-contact',
            'capability' => 'edit_posts',
            'parent_slug' => 'bq-angular',
            'position' => false,
            'icon_url' => false
        ]);

    }

    public function loadVendorLibraries() {
        // Retrieve the context director
        $dir = untrailingslashit( plugin_dir_path(__FILE__ ) );

        if (!class_exists('PHPMailerAutoload')) {
            // Include the phpmailer autloader
            require_once($dir.'/vendors/phpmailer/PHPMailerAutoload.php');
        }

        if (!class_exists('GUMP')) {
            // Include the phpmailer autloader
            require_once($dir.'/vendors/gump/gump.class.php');
        }
    }

    public function loadCoreClasses() {
        // Retrieve the context director
        $dir = untrailingslashit( plugin_dir_path(__FILE__ ) );
        // Recurssively load the directory
        $this->_Recusive_Load_Dir($dir.'/core');
    }

    public function loadWidgetHandlers() {
        // Retrieve the context director
        $dir = untrailingslashit( plugin_dir_path(__FILE__ ) );
        // Recurssively load the directory
        $this->_Recusive_Load_Dir($dir.'/widgets');

        add_action('init', function(){
            // Retrieve the php input
            $input = file_get_contents('php://input');
            // If no php input was passed
            if (!$input) { return; }
            // Potentially decode the provided input
            $json = json_decode($input, true);
            // If no json was passed or it is not an widget
            if (!is_array($json) || !isset($json['widget']) || !isset($json['action'])) { return; }
            // If the widget does not exist
            if (!$this->widgets[$json['widget']]) { return; }
            // Retrieve the handler
            $handler = $this->widgets[$json['widget']]['handler'];
            // Create a new widget class
            $_widget = new $handler();
            // Do the widget
            $_widget->doAction($json['action'], $json['data']);
        }); return;

    }

    public function loadJsScripts() {
        // Include the public facing js libraries
        add_action('wp_enqueue_scripts',function(){
            // Add in jQuery ui libs
            wp_enqueue_script('jquery-ui-core', ['jquery']);
            wp_enqueue_script('jquery-ui-datepicker', ['jquery']);
            // Include the required angular libraries
            wp_enqueue_script('ng-1', '/ng-build/node_modules/core-js/client/shim.min.js', [], '20120608', false);
            wp_enqueue_script('ng-2', '/ng-build/node_modules/zone.js/dist/zone.js', [], '20120608', false);
            wp_enqueue_script('ng-3', '/ng-build/node_modules/reflect-metadata/Reflect.js', [], '20120608', false);
            wp_enqueue_script('ng-4', '/ng-build/node_modules/systemjs/dist/system.src.js', [], '20120608', false);
            wp_enqueue_script('ng-5', '/ng-build/node_modules/ng2-bootstrap/bundles/ng2-bootstrap.min.js', [], '20120608', false);
            // Include opouch db for local database
            wp_enqueue_script('ng-6', '/ng-build/vendors/jquery_input_mask/dist/jquery.inputmask.bundle.js', [], '20120608', true);
            wp_enqueue_script('ng-7', '/ng-build/vendors/jquery_ui/jquery-ui-timepicker-addon.min.js', [], '20120608', true);
            // Include the main application
            wp_enqueue_script('ng-9', '/ng-app/systemjs.config.js', [], '20120608', false);
            // Include the application.js
            wp_enqueue_script('ng-10', '/ng-app/application.js', [], '20120608', false);
        });
        // Include the administration libraries
        add_action('admin_enqueue_scripts',function() {

        });
    }

    public function loadCssScripts() {
        // Include the public facing css libraries
        add_action('wp_enqueue_scripts', function() {
            // font awesome
            wp_enqueue_style('bg-angular', plugins_url('assets/bq.angular.css', __FILE__ ));
        });
    }

    public function loadShortCodes() {
        // Add the ng shortcode
        add_shortcode('ng', function($atts){
            // Retrieve the shortcode attributes
            $a = shortcode_atts( array(
                'widget' => 'widget'
            ), $atts );
            // If no widget could be found
            if (!isset($this->widgets[$a['widget']])) { return 'WIDGET "'.$a['widget'].'" NOT FOUND'; }
            // Retrieve the widget data
            $widget = $this->widgets[$a['widget']];
            // Return the shortcode HTML
            return '
                <'.$widget['ng']['selector'].'>Loading</'.$widget['ng']['selector'].'>
                <script type="text/javascript">
                    // Import the main application and bing the error
                    System.import(\''.$widget['ng']['system'].'\')
                            .then(null, console.error.bind(console));
                </script>
            ';
        });
    }

    protected function _Recusive_Load_Dir($dir) {
        // If the directory doesn't exist
        if (!is_dir($dir)) { return; }
        // Load each of the field shortcodes
        foreach (new DirectoryIterator($dir) as $FileInfo) {
            // If this is a directory dot
            if ($FileInfo->isDot()) { continue; }
            // If this is a directory
            if ($FileInfo->isDir()) {
                // Load the directory
                $this->_Recusive_Load_Dir($FileInfo->getPathname());
            } // Otherwise load the file
            else {
                // If this is not false
                if (stripos($FileInfo->getFilename(),'.tpl') !== false) { continue; }
                // If this is not false
                if (stripos($FileInfo->getFilename(),'.php') === false) { continue; }
                // Include the file
                require_once($FileInfo->getPathname());
            }
        }
    }

}

new BQ_Angular();