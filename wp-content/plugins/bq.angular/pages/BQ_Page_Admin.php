<?php

//Our class extends the WP_List_Table class, so we need to make sure that it's there
if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class BQ_Page_Admin {

    public $plugin;

    public function renderPage() {

        switch ($_GET['action']) {
            case 'edit' : echo $this->renderManagePage(); break;
            default : echo $this->renderIndexPage(); break;
        }
    }

    public function renderIndexPage() {
        //Prepare Table of elements
        $wp_list_table = new BQ_Admin_Table();

        $wp_list_table->items = $this->plugin->widgets;

        $wp_list_table->prepare_items();
        // Retrieve the context director
        $tmpl_dir = BQ_ANGULAR_DIR.'/templates';
        // Start gathering content
        ob_start();
        // Include the template file
        include($tmpl_dir.'/admin.page.index.php');
        // Get the html contents
        $html = ob_get_contents();
        // Clean up
        ob_end_clean();
        // Return the rendered HTML
        return $html;
    }

    public function renderManagePage() {
        // Retrieve the requested widget
        $g_widget = $_GET['widget'];
        // If no widget was passed or the widget does not exist
        if (!$g_widget || !isset($this->plugin->widgets[$g_widget])) { return 'No widget found'; }
        // Retrieve the widget data
        $d_widget = $this->plugin->widgets[$g_widget];
        // Retriever the widget handler
        $h_widget = new $d_widget['handler']();
        // Return the rendered manage page
        return $h_widget->getSettingsPage();
    }
}

class BQ_Admin_Table extends WP_List_Table {

    public $items;

    /**
     * Define the columns that are going to be used in the table
     * @return array $columns, the array of columns to use with the table
     */
    function get_columns() {

        return [
            'title' => __('Widget Title'),
            'description' => __('Description'),
            'shortcode' => __('Shortcode'),
        ];
    }

    /**
     * Decide which columns to activate the sorting functionality on
     * @return array $sortable, the array of columns that can be sorted by the user
     */
    public function get_sortable_columns() {

        return [];
    }

    /**
     * Prepare the table with different parameters, pagination, columns and table elements
     */
    function prepare_items() {

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = array();
        $this->_column_headers = array($columns, $hidden, $sortable);
    }

    function column_default( $item, $column_name ) {
        switch( $column_name ) {
            case 'title': return $item['title']; break;
            case 'description': return $item['description']; break;
            case 'shortcode': return '[ng widget="'.$item['code'].'"]'; break;
            default: return print_r( $item, true ) ;
        }
    }

    function column_title( $item) {

        $actions = array(
            'edit' => sprintf('<a href="?page=%s&action=%s&widget=%s">Edit Widget</a>',$_REQUEST['page'],'edit',$item['code'])
        );

        return sprintf('%1$s %2$s', $item['title'], $this->row_actions($actions) );

    }


}