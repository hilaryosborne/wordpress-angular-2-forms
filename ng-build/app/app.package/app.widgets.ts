import {NgModule, Directive, Input, ElementRef, Renderer, HostBinding} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {HostListener} from "@angular/core";

@Directive({ selector: '[toggleCollectionDirective]' })
export class AppToggleCollectionDirective {

    public intervalCheck;

    @Input('toggleCollection')
    public toggleCollection;

    @Input('toggleCollectionValue')
    public toggleCollectionValue;

    @HostBinding('class.btn-primary') is_selected = false;

    @HostBinding('class.btn-default') is_unselected = false;

    constructor(private el: ElementRef, private renderer: Renderer) {

        var self = this;

        self.intervalCheck = setInterval(function(){ self.doValueCheck(); }, 50);
    }

    @HostListener('click', ['$event'])
    public doToggleCollection() {

        var self = this;

        var index = self.toggleCollection.indexOf(self.toggleCollectionValue);

        if (index < 0) {

            self.toggleCollection.push(self.toggleCollectionValue);
        }
        else { self.toggleCollection.splice(index,1); }
    }

    protected doValueCheck() {
        var self = this;

        if (self.toggleCollection.indexOf(self.toggleCollectionValue) < 0) {
            self.is_unselected = true; self.is_selected = false;
        } else {
            self.is_unselected = false; self.is_selected = true;
        }
    }

}

@Directive({ selector: '[disableIfHidden]' })
export class AppDisableIfHiddenDirective {

    public intervalCheck;

    @Input('disableIfHidden')
    public disableIfHidden;

    constructor(
        protected el: ElementRef,
        protected renderer: Renderer) {}

    public ngOnInit() {

        var self = this;

        self.intervalCheck = setInterval(function(){ self.doVisibilityCheck(); }, 200);
    }

    public doVisibilityCheck() {

        var self = this;

        if (!self.el.nativeElement.offsetParent) {
            self.disableIfHidden.disable();
        } else {
            self.disableIfHidden.enable();
        }
    }

    public ngOnDestroy() {

        var self = this;

        clearInterval(self.intervalCheck);
    }

}


@NgModule({
    imports: [ BrowserModule],
    declarations: [ AppDisableIfHiddenDirective, AppToggleCollectionDirective ],
    exports: [ AppDisableIfHiddenDirective, AppToggleCollectionDirective ],
    providers:[ ]
})
export class AppWidgetsModule { }